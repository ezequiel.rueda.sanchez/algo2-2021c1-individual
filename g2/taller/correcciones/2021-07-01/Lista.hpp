#include "Lista.h"

Lista::Lista() {
    _primero = nullptr;  /** El puntero al primer nodo es nulo porque no tenemos elementos en la lista */
    _ultimo = nullptr;  /** El puntero al ultimo nodo es nulo porque no tenemos elementos en la lista */
}

Lista::Lista(const Lista& l) : Lista() {
    /** Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista. */
    *this = l;
}

Lista::~Lista() {
    while (_primero != nullptr){
        sacarPrimero();
    }
}

void Lista::sacarPrimero() {
    Nodo* head = _primero;
    _primero = _primero->siguiente;
    delete head;
}

Lista& Lista::operator=(const Lista& aCopiar) {
    borrar();                                       /** Borramos todos los elementos de la lista para poder agregar los de la lista "aCopiar" */
    Nodo* nodo_actual = aCopiar._primero;          /**  Apuntamos nodo_actual al primer nodo de la lista "aCopiar"  */
    while (nodo_actual != nullptr){               /**   Recorremos la lista "aCopiar"  */
        this->agregarAtras(nodo_actual->valor);  /**    Agregamos los elementos de la lista "aCopiar" en "this"  */
        nodo_actual = nodo_actual->siguiente;   /**     Avanzamos al siguiente nodo de la lista "aCopiar"  */
    }
     return *this;                             /**      Devolvemos la lista "aCopiar" con todos los elementos copiados  */
}

void Lista::borrar() {
    while (_primero != nullptr){
        sacarPrimero();
    }
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* nodo_nuevo_ad = new Nodo(elem);               /** Cremos el nodo a insertar adelante  */
    /** Caso 1: No hay nodos en la lista  */
    if(_primero == nullptr){
        _primero = nodo_nuevo_ad;
        _ultimo = nodo_nuevo_ad;
        /** Caso 2: Tenemos unicamente un nodo en la lista  */
    } else if (_primero == _ultimo) {
        _primero->anterior = nodo_nuevo_ad;
        nodo_nuevo_ad->siguiente = _ultimo;
        _primero = nodo_nuevo_ad;
        /** Caso 3: Tenemos mas de un nodo en la lista  */
    } else {
        _primero->anterior = nodo_nuevo_ad;
        nodo_nuevo_ad->siguiente = _primero;
        _primero = nodo_nuevo_ad;
    }
}


void Lista::agregarAtras(const int& elem) {
    Nodo* nodo_nuevo_at = new Nodo(elem);                         /** Cremos el nodo a insertar atras  */
    /** Caso 1: No hay nodos en la lista  */
    if(_primero == nullptr){
        _primero = nodo_nuevo_at;
        _ultimo = nodo_nuevo_at;
        /** Caso 2: Tenemos unicamente un nodo en la lista  */
    } else if (_primero == _ultimo) {
        _primero->siguiente = nodo_nuevo_at;
        nodo_nuevo_at->anterior = _primero;
        _ultimo = nodo_nuevo_at;
        /** Caso 3: Tenemos mas de un nodo en la lista  */
    } else {
        _ultimo->siguiente = nodo_nuevo_at;
        nodo_nuevo_at->anterior = _ultimo;
        _ultimo= nodo_nuevo_at;
    }
}

void Lista::eliminar(Nat i) {
    Nodo* nodo_actual = _primero; /** Creamos un puntero apuntando al primer nodo de la lista  */
       /** Recorremos el ciclo i veces hasta encontrar la posicion del elementno a eliminar   */
    while (i > 0) {
        i--;
        nodo_actual = nodo_actual->siguiente;
    }
    Nodo* nodo_ant_actual = nodo_actual->anterior;
    Nodo* nodo_sig_actual = nodo_actual->siguiente;
    /** Caso 1: La lista tiene unicamente un solo elemento y lo quiero borrar  */
    if (nodo_ant_actual == nullptr && nodo_sig_actual == nullptr) {
        _primero = nullptr;
        _ultimo = nullptr;
        delete nodo_actual;
    }
    /** Caso 2: Quiero borrar el primer elemento de la lista  */
    if (nodo_ant_actual == nullptr && nodo_sig_actual != nullptr) {
        _primero = nodo_sig_actual;
        nodo_sig_actual->anterior = nullptr;
        delete nodo_actual;
    }
    /** Caso 3: Quiero borrar el ultimo elemento de la lista  */
    if (nodo_ant_actual != nullptr && nodo_sig_actual == nullptr) {
        _ultimo = nodo_ant_actual;
        nodo_ant_actual->siguiente = nullptr;
        delete nodo_actual;
    }
    /** Caso 4: Quiero borrar un elemento que no sea ni el primero ni el ultimo de la lista  */
    if (nodo_ant_actual != nullptr && nodo_sig_actual != nullptr) {
        nodo_ant_actual->siguiente = nodo_sig_actual;
        nodo_sig_actual->anterior = nodo_ant_actual;
        delete nodo_actual;
    }
}

int Lista::longitud() const {
    int tam = 0;
    Nodo* temp = _primero;
    while (temp != nullptr){
            tam++;
            temp = temp->siguiente;
        }
    return tam;
}




const int& Lista::iesimo(Nat i) const {
    Nodo* nodo_actual = _primero;
    while (i > 0){
        i--;
        nodo_actual = nodo_actual->siguiente;
    }
    return nodo_actual->valor;
}


int& Lista::iesimo(Nat i) {
    Nodo* nodo_actual = _primero;
    while (i > 0){
        i--;
        nodo_actual = nodo_actual->siguiente;
    }
    return nodo_actual->valor;
}

void Lista::mostrar(ostream& o) {
    Nodo* actual = _primero;
    while (actual != nullptr){
        o << actual->valor << endl;
        actual = actual->siguiente;
    }
}
