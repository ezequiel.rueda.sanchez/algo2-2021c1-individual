#include "Lista.h"

Lista::Nodo::Nodo(const int& elem) : valor(elem), anterior(nullptr), siguiente(nullptr) {};

Lista::Lista() {
    _primero = nullptr; //la flecha que apunta al primer nodo es un puntero nulo porque no tenemos elementos en la lista
    _ultimo = nullptr;  //la flecha que apunta al ultimo nodo es un puntero nulo porque no tenemos elementos en la lista
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    while (_primero != nullptr){
        sacarPrimero();
    }
}

void Lista::sacarPrimero() {
    Nodo* head = _primero;
    _primero = _primero->siguiente;
    delete head;
}

Lista& Lista::operator=(const Lista& aCopiar) {
    borrar();                                           // Borramos todos los elementos de la lista para poder agregar los de la lista "aCopiar"
    Nodo* nodo_actual = aCopiar._primero;              //  El puntero  nodo_actual esta apuntando al primer nodo de la lista "aCopiar"
    while (nodo_actual != nullptr){                   //   Recorremos la lista "aCopiar"
        this->agregarAdelante(nodo_actual->valor);   //    Vamos agregando los elementos de la lista "aCopiar" en "this"
        nodo_actual = nodo_actual->siguiente;       //     Avanzamos al siguiente nodo de la lista "aCopiar"
    }
     return *this;                                  //     Devolvemos la lista "aCopiar" con todos los elementos copiados
}

void Lista::borrar() {
    while (_primero != nullptr){
        sacarPrimero();
    }
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* nodo_nuevo_ad = new Nodo(elem);     // Reservamos una direccion de memoria que va a apuntar al nodo con el elemento que queremos insertar
    nodo_nuevo_ad->anterior = nullptr;       //  Como el elemento a insertar sera el primero, la flecha al nodo anterior apunta a una direccion vacia
    nodo_nuevo_ad->siguiente = _primero;    //   La flecha al nodo siguiente apunta a la direccion de memoria donde estaba el primer nodo
    _primero = nodo_nuevo_ad;              //    Ahora, la flecha al primer nodo apunta a la direccion de memoria donde insertamos el nodo
}

void Lista::agregarAtras(const int& elem) {
    Nodo* nodo_nuevo_at = new Nodo(elem);  // Reservamos una direccion de memoria que va a apuntar al nodo con el elemento que queremos insertar
    nodo_nuevo_at->siguiente = nullptr;   //  Como el elemento a insertar sera el ultimo, la flecha al nodo siguiente apunta a una direccion vacia
    nodo_nuevo_at->anterior= _ultimo;    //   La flecha al nodo anterior apunta a la direccion de memoria donde estaba el ultimo nodo
    _ultimo = nodo_nuevo_at;            //    Ahora, la flecha al ultimo nodo apunta a la direccion de memoria donde insertamos el nodo
}

void Lista::eliminar(Nat i) {
    Nodo* nodo_actual = _primero;
    while (i > 0){
        i--;
        nodo_actual = nodo_actual->siguiente;
    }
    nodo_actual->anterior->siguiente = nodo_actual->siguiente;
    nodo_actual->siguiente->anterior = nodo_actual->anterior;
    delete nodo_actual;
}

int Lista::longitud() const {
    int tamanio = 0;
    Nodo* temp = _primero;
    while (temp != nullptr){
        tamanio++;
        temp = temp->siguiente;
    }
    return tamanio;
}

const int& Lista::iesimo(Nat i) const {
        Nodo* nodo_actual = _primero;
        const int& res = _primero->valor;
        while (i > 0){
            i--;
            nodo_actual = nodo_actual->siguiente;
        }
        res = nodo_actual->valor;
        return res;
}

int& Lista::iesimo(Nat i) {
    Nodo* nodo_actual = _primero;
    int& res = _primero->valor;
    while (i > 0){
        i--;
        nodo_actual = nodo_actual->siguiente;
    }
    res = nodo_actual->valor;
    return res;
}

void Lista::mostrar(ostream& o) {
    Nodo* actual = _primero;
    while (actual != nullptr){
        o << actual->valor << endl;
        actual = actual->siguiente;
    }
}
