template <typename T>
string_map<T>::string_map() : raiz(nullptr), _size(0) {}
/** Constructor. Inicializamos un Trie vacio */

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() {
    *this = aCopiar; /** Provisto por la catedra: utiliza el operador asignacion para realizar la copia. */
}

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    destruir(raiz);
    _size = 0;
    for(int i = 0; i < d.dicc2.size(); i++) {
        insert(d.dicc2[i]);
        _size++;
    }
}

template <typename T>
vector<pair<string, T>> string_map<T>::dicc() {
    return dicc2;
}

template <typename T>
string_map<T>::~string_map(){
    destruir(raiz);
}

template <typename T>
void string_map<T>::destruir(Nodo* r) {
    if (r != nullptr){
        for(int i = 0;i < 256; i++){
            Nodo* letra = r->siguientes[i];
            destruir(letra);
        }
        delete r;
    }
}

template <typename T>
void string_map<T>::insert(const pair<string, T>& a) {
    /** Caso 1: El Trie se encuentra vacio. Entonces, inicialiazamos un nuevo nodo y la raiz apunta a el **/
    if (raiz == nullptr){
        Nodo* nodo_nuevo = new Nodo();
        raiz = nodo_nuevo;
    }
    /** Coso 2: El Trie NO se encuentra vacio. Vamos a insertar la clave con su valor **/
    Nodo* nodo_actual = raiz;
    for (int i = 0; i < a.first.size(); i++) {
        for (int j = 0; j < 256; j++) {
            /** Chequeamos si el i-esimo caracter al solicitar la funcion int, que devuelve su codigo numerico asociado, coinicide con la posicion del vector siguientes. */
            if (j == int(a.first[i])) {
                if(nodo_actual->siguientes[j] == nullptr){
                    /** Si su nodo correspondiente apunta a nulo, creamos uno nuevo donde el puntero en la posicion j apunta al nodo creado para buscar la siguiente letra de la clave */
                    Nodo* aux = new Nodo();
                    nodo_actual->siguientes[j] = aux;
                }
                /** Continuamos con la insercion de la siguiente letra de la clave y colocamos a j en 256 asi podemos pasar a la siguiente letra mas rapidamente */
                nodo_actual = nodo_actual->siguientes[j];
                j = 256;
            }
        }
    }
    _size++;
    nodo_actual->definicion = a.second;
    nodo_actual->status = true;
    dicc2.push_back(a);
}



template <typename T>
int string_map<T>::count(const string& key) const {
    Nodo* nodo_actual = raiz;
    if (_size == 0){
        return 0;
    }
    /** Caso 2: El Trie NO se encuentra vacio. Por lo tanto, vemos si encontramos la clave key. */
    for(int i = 0; i < key.size();i++){
        for(int j = 0; j < 256; j++){
            if (j == int(key[i])){
                if (nodo_actual != nullptr){
                    nodo_actual = nodo_actual->siguientes[j];
                }
                j = 256;
            }
        }
    }
    /** Ya recorrimos toda la clave key y verificamos si se encuentra en el Trie . */
    if (nodo_actual != nullptr){
        if (nodo_actual->status == true){
            return 1;
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}

/** PRE: Sabemos que la clave key se encuentra definida en el Trie. */
template <typename T>
const T& string_map<T>::at(const string& key) const {
    Nodo* nodo_actual = raiz;
    for(int i = 0; i < key.size();i++) {
        for(int j = 0; j < 256; j++) {
            if (j == int(key[i])) {
                nodo_actual = nodo_actual->siguientes[j];
            }
                j = 256;
            }
        }
    return nodo_actual->definicion;
}

/** PRE: Sabemos que la clave key se encuentra definida en el Trie. */
template <typename T>
T& string_map<T>::at(const string& key) {
    Nodo* nodo_actual = raiz;
    for(int i = 0; i < key.size();i++) {
        for(int j = 0; j < 256; j++) {
            if (j == int(key[i])) {
                nodo_actual = nodo_actual->siguientes[j];
                j = 256;
            }
        }
    }
    return nodo_actual->definicion;
}

/** PRE: Sabemos que la clave se encuentra definida en el Trie. */
template <typename T>
void string_map<T>::erase(const string& key) {
/** Creamos un diccionario auxiliar donde vamos a ir guardando todas las claves distintas a key, que es la queremos borrar. */
    vector<pair<string,T>> diccaux;
    for(int i = 0; i < dicc2.size(); i++) {
        if(dicc2[i].first != key)
            diccaux.push_back(dicc2[i]);
    }
    /** Ahora, diccDos vuelve a ser el diccionario original pero sin la clave a eliminar con su respectivo significado. */
    dicc2 = diccaux;
    Nodo* nodo_actual = raiz;
    Nodo* nodo_aux = raiz;
    int last = 0;
    for(int i = 0; i < key.size();i++) {
        for(int j = 0; j < 256; j++) {
            if(hijos(nodo_actual) || nodo_actual->status || nodo_actual == raiz) {
                nodo_aux = nodo_actual;
                if (j == int(key[i])){
                    last = j;
                }
            }
            if (j == int(key[i])) {
                nodo_actual = nodo_actual->siguientes[j];
                j = 256;
            }
        }
    }
    _size--;
    nodo_actual->status = false;
    Nodo* nodo_borrar = nodo_aux->siguientes[last];
    if (!hijo(nodo_actual)) {
        destruir(nodo_borrar);
        nodo_aux->siguientes[last] = nullptr;
    }
}


template <typename T>
bool string_map<T>::hijos(Nodo const* r) {
    int acum = 0;
    for (int i = 0; i < 256; i++) {
        if (r->siguientes[i] != nullptr){
            acum++;
        }
    }
    if (acum > 1){
        return true;
    } else {
        return false;
    }
}

template <typename T>
bool string_map<T>::hijo(Nodo const* r) {
    for (int i = 0; i < 256; i++) {
        if (r->siguientes[i] != nullptr){
            return true;
        }
    }
    return false;
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    return _size == 0;

}

template <typename T>
T& string_map<T>::operator[](const string& key){
    Nodo* nodo_actual = raiz;
    for(int i = 0; i < key.size();i++) {
        for(int j = 0; j < 256; j++) {
            if (j == int(key[i])) {
                nodo_actual = nodo_actual->siguientes[j];
                j = 256;
            }
        }
    }
    return nodo_actual->definicion;
}
