#include <iostream>
#include <vector>
#include <list>
using namespace std;

using uint = unsigned int;



// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
            // ene, feb, mar, abr, may, jun
            31,  28,  31, 30, 31,    30,
            // jul, ago, sep, oct, nov, dic
            31,  31,  30,  31,  30,  31
    };
    return dias[mes + 1];
}

// Ejercicio 7

// Clase Fecha

class Fecha {
public:
    Fecha(int mes, int dia);
    uint mes();
    uint dia();
    bool operator==(Fecha f);
    void incrementar_dia();

private:
    uint mes_;
    uint dia_;
};

Fecha:: Fecha(int mes, int dia) : mes_(mes), dia_(dia) {};

uint Fecha::dia(){
    return dia_;
}

uint Fecha::mes(){
    return mes_;
}

// Ejercicio 8

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << " / " << f.mes();
    return os;
}

// Ejercicio 9

#if EJ >= 9
bool Fecha::operator==(Fecha f) {
    bool igual_dia = dia_ == f.dia();
    bool igual_mes = mes_ == f.mes();
    return igual_dia && igual_mes;
}
#endif

// Ejercicio 10

void Fecha::incrementar_dia() {
    if(dias_en_mes(mes_) == dia_){
        mes_++;
        dia_ = 1;
    } else {
        dia_++;
    }
}

// Ejercicio 11

class Horario {
public:
    Horario(uint hor, uint min);
    uint hora();
    uint min();
    bool operator==(Horario h);
    bool operator<(Horario h);

private:
    uint hora_;
    uint min_;
};

Horario:: Horario(uint hor, uint min) : hora_(hor), min_(min) {};

uint Horario::hora(){
    return hora_;
}

uint Horario::min(){
    return min_;
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << " : " << h.min();
    return os;
}

bool Horario::operator==(Horario h) {
    bool igual_horario = (hora_ == h.hora()) and (min_ == h.min());
    return igual_horario;
}

// Ejercicio 12

bool Horario::operator<(Horario h) {
    bool menor_horario = true;
    if ((h.hora() > hora_) or (h.hora() == hora_ and h.min() > min_)){
        menor_horario = false;
    }
    return menor_horario;
}



// Ejercicio 13

class Recordatorio {
public:
    Recordatorio(Fecha f, Horario h, string s);
    Fecha date();
    Horario time();
    string mensaje();

private:
    Fecha date_;
    Horario time_;
    string mensaje_;
};

Recordatorio::Recordatorio(Fecha f, Horario h, string s) : date_(f), time_(h), mensaje_(s) {};

Fecha Recordatorio::date(){
    return date_;
}

Horario Recordatorio::time(){
    return time_;
}

string Recordatorio::mensaje(){
    return mensaje_;
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.mensaje() << " @ " << r.date() << r.time();
    return os;
}

// Ejercicio 14

class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();

private:
    Fecha hoy_;
    map<Fecha,list<Recordatorio>> agenda_;
};

Agenda::Agenda(Fecha fecha_inicial) :  hoy_(fecha_inicial) {};

void Agenda::agregar_recordatorio(Recordatorio rec){
    for(pair<Fecha,list<Recordatorio>> pflr : agenda_){
        if ( pflr.first == rec.date()){
            pflr.second.push_back(rec);
        }
    }
    list<Recordatorio> nuevoRecordatorio;
    nuevoRecordatorio.push_back(rec);
    agenda_.insert(make_pair(rec.date(),nuevoRecordatorio));
}

void Agenda::incrementar_dia(){
    return hoy_.incrementar_dia();
}

list<Recordatorio> Agenda::recordatorios_de_hoy(){
    list<Recordatorio> records_de_hoy;
    for(pair<Fecha,list<Recordatorio>> pflr : agenda_){
        if ( pflr.first == hoy_){
            records_de_hoy = pflr.second;
        }
    }
    return records_de_hoy;
}

Fecha Agenda::hoy(){
    return hoy_;
}


ostream& operator<<(ostream& os, Agenda a) {
    os << a.hoy() << endl;
    os << " ===== " << endl;
    list<Recordatorio> ag = a.recordatorios_de_hoy(); //Redefino toda la lista de recordatorios con el nombre ag//
    vector<Recordatorio> recordatorios; //Creo un vector con los recordatorios para poder recorrerlo, comparar los horarios y ordenarlos//
    for(Recordatorio r : ag) {
        recordatorios.push_back(r);
    }
    vector<Recordatorio> recordatorios_en_horario; //Creo un vector vacio con los recordatorios con sus respectivos horarios ordenados//
    for(int i = 0; i < recordatorios.size(); i++) {
        /* En el siguiente ciclo, voy a recorrer todo el vector con los recordatorios para encontrar en cada iteracion i, la posicion donde esta el menor horario */
        int pos_minHor = i;
        for (int j = i + 1; j < recordatorios.size(); j++) {
            if (recordatorios[j].time() < recordatorios[pos_minHor].time()) {
                pos_minHor = j;
            }
        }
        recordatorios_en_horario.push_back(recordatorios[pos_minHor]); //Coloco en el vector que voy a imprimir,el recordatorio con el minimo horario//
        //recordatorios = intercambiar(recordatorios,i,pos_minHor); //Intercambio la posicion del minimo horario con la i esima posicion que estoy iterando y avanzo i//
    }
    for(Recordatorio recor : recordatorios_en_horario){ //Recorro el vector de los recordatorios con sus horarios ordenados y lo imprimo por pantalla//
        os << recor << endl;
    }
    return os;
}

void intercambiar(vector<Recordatorio> &lr, int m, int n) {
    lr[m] = lr[n];
    lr[n] = lr[m];
}

